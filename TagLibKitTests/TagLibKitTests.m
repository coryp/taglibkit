//
//  TagLibKitTests.m
//  TagLibKitTests
//
//  Created by Cory Powers on 5/1/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import "TagLibKitTests.h"
#import "TKFile.h"

@interface TagLibKitTests ()
@property (nonatomic, strong) NSString *testVideoFile;
@property (nonatomic, strong) TKFile *writeTaglibFile;
@property (nonatomic, strong) TKFile *readTaglibFile;
@property (nonatomic, strong) NSImage *testCoverArt;
@end

@implementation TagLibKitTests

- (void)setUp {
    [super setUp];
    
	NSBundle *myBundle = [NSBundle bundleWithIdentifier:@"com.avici.TagLibKitTests"];
	
	self.testVideoFile = [myBundle pathForResource:@"TestVideo" ofType:@"m4v"];
	NSData *coverArtData = [NSData dataWithContentsOfFile:[myBundle pathForResource:@"TestCoverArt" ofType:@"png"]];
 	self.testCoverArt = [[NSImage alloc] initWithData:coverArtData];
	
	NSLog(@"Test file: %@", self.testVideoFile);
	
	STAssertNotNil(self.testVideoFile, @"testVideoFile is nil");

	self.writeTaglibFile = [[TKFile alloc] initWithFile:self.testVideoFile];
	
	self.readTaglibFile = [[TKFile alloc] initWithFile:self.testVideoFile];
}

- (void)tearDown {
    // Tear-down code here.
	self.testVideoFile = nil;
	self.writeTaglibFile = nil;
	self.readTaglibFile = nil;
    
    [super tearDown];
}

- (void)testWriteShowName {
	NSString *testShowName = @"My Show Name";
	
	self.writeTaglibFile.showName = testShowName;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.showName, testShowName, @"Show name read does not match expected show name");
}

- (void)testWriteSeasonNumber {
	NSInteger seasonNumber = 5;

	self.writeTaglibFile.season = seasonNumber;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEquals(self.readTaglibFile.season, seasonNumber, @"Season number read does not match number written");
}

- (void)testWriteEpisodeNumber {
	NSInteger episodeNumber = 23;
	
	self.writeTaglibFile.episode = episodeNumber;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEquals(self.readTaglibFile.episode, episodeNumber, @"Episode number does not match number written");
}

- (void)testWriteCoverArt {	
	self.writeTaglibFile.coverArt = self.testCoverArt;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertNotNil(self.readTaglibFile.coverArt, @"Cover art is nil");
}

- (void)testWriteMediaType {
	enum TKMediaType testMediaType = TKMediaTypeTVShow;
	
	self.writeTaglibFile.mediaType = testMediaType;
	
	[self.writeTaglibFile writeTags];
		
	[self.readTaglibFile readTags];
	
	STAssertEquals(self.readTaglibFile.mediaType, testMediaType, @"Media type does not match");
}

- (void)testWriteHDVideo {
	self.writeTaglibFile.hdVideo = YES;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEquals(self.readTaglibFile.hdVideo, YES, @"HD Video tag does not match");
}

- (void)testWriteReleaseDateLongFormat {
	NSDate *testDate = [NSDate dateWithString:@"2013-04-23 14:07:00 +00:00"];
	
	self.writeTaglibFile.releaseDate = testDate;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.releaseDate, testDate, @"Release date does not match");
}

- (void)testWriteEpisodeDescription {
	NSString *testDescription = @"This is a description for My Show Name episode 23";
	
	self.writeTaglibFile.episodeDescription = testDescription;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.episodeDescription, testDescription, @"Description does not match");
}

- (void)testWriteLongDescription {
	NSString *testDescription = @"This is a description for My Show Name episode 23 which is longer than the previous description";
	
	self.writeTaglibFile.longDescription = testDescription;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.longDescription, testDescription, @"Long Description does not match");
}

- (void)testWriteShowDescription {
	NSString *testDescription = @"This is a general description for My Show Name which is not specific to an episode";
	
	self.writeTaglibFile.showDescription = testDescription;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.showDescription, testDescription, @"Show Description does not match");
}

- (void)testWriteNetwork {
	NSString *testNetwork = @"Cory's Broadcast Network";
	
	self.writeTaglibFile.network = testNetwork;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.network, testNetwork, @"Network does not match");
}

- (void)testWriteEpisodeID {
	NSString *testNetwork = @"523";
	
	self.writeTaglibFile.network = testNetwork;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.network, testNetwork, @"Network does not match");
}

- (void)testWriteTitle {
	NSString *testTitle = @"Bob and bill go to work";
	
	self.writeTaglibFile.title = testTitle;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.title, testTitle, @"Title does not match");
}

- (void)testWriteCopyright {
	NSString *testCopyright = @"2007 Cory's Broadcast Network";
	
	self.writeTaglibFile.copyright = testCopyright;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.copyright, testCopyright, @"Copyright does not match");
}

- (void)testWriteGenre {
	NSString *testGenre = @"Comedy";
	
	self.writeTaglibFile.genre = testGenre;
	
	[self.writeTaglibFile writeTags];
	
	[self.readTaglibFile readTags];
	
	STAssertEqualObjects(self.readTaglibFile.genre, testGenre, @"Genre does not match");
}

@end
