//
//  TKFile.m
//  TagLibKit
//
//  Created by Cory Powers on 5/1/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import "TKFile.h"

#include <iostream>
#include <iomanip>
#include <stdio.h>

#include "taglib/fileref.h"
#include "taglib/tag.h"
#include "taglib/mp4file.h"
#include "taglib/mp4coverart.h"
#include "taglib/tpropertymap.h"

@interface TKFile ()
@property (nonatomic, strong) NSString *filePath;

- (NSString *)stringFromMP4Item: (TagLib::MP4::Item)anItem;
@end

@implementation TKFile

- (id)initWithFile: (NSString *)filePath {
	self = [super init];
	if (self) {
		self.filePath = filePath;
		self.mediaType = TKMediaTypeUnknown;
	}
	
	return self;
}

- (void)readTags {
	TagLib::MP4::File mp4File([self.filePath cStringUsingEncoding:NSUTF8StringEncoding]);

	if (!mp4File.isValid()) {
		NSLog(@"Could not open file %@", self.filePath);
		return;
	}
	
	TagLib::MP4::Tag *tag = mp4File.tag();

	TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
	for (TagLib::MP4::ItemListMap::ConstIterator i = itemsListMap.begin(); i != itemsListMap.end(); ++i) {
		TagLib::MP4::Item item = i->second;
		NSString *itemName = [NSString stringWithCString:i->first.toCString(true) encoding:NSUTF8StringEncoding];
		
		if ([itemName isEqualToString:@"covr"]) {
			TagLib::MP4::CoverArtList coverArtList = i->second.toCoverArtList();
			for (TagLib::MP4::CoverArtList::ConstIterator j = coverArtList.begin(); j != coverArtList.end(); ++j) {
				NSData *imageData = [NSData dataWithBytes:j->data().data() length:j->data().size()];
				self.coverArt = [[NSImage alloc] initWithData:imageData];
			}
		}else if ([itemName isEqualToString:@"stik"]) {
			NSInteger mediaTypeID = i->second.toInt();
			if (mediaTypeID == 1) {
				self.mediaType = TKMediaTypeMusic;
			}else if (mediaTypeID == 2) {
				self.mediaType = TKMediaTypeAudiobook;
			}else if (mediaTypeID == 6) {
				self.mediaType = TKMediaTypeMusicVideo;
			}else if (mediaTypeID == 9) {
				self.mediaType = TKMediaTypeMovie;
			}else if (mediaTypeID == 10) {
				self.mediaType = TKMediaTypeTVShow;
			}else if (mediaTypeID == 11) {
				self.mediaType = TKMediaTypeBooklet;
			}else if (mediaTypeID == 14) {
				self.mediaType = TKMediaTypeRingtone;
			}
		}else if ([itemName isEqualToString:@"©nam"]){
			self.title = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"cprt"]){
			self.copyright = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"©gen"]){
			self.genre = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"tvsh"]){
			self.showName = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"desc"]){
			self.episodeDescription = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"ldes"]){
			self.longDescription = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"sdes"]){
			self.showDescription = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"tvnn"]){
			self.network = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"tven"]){
			self.episodeID = [self stringFromMP4Item:i->second];
		}else if ([itemName isEqualToString:@"tvsn"]){
			self.season = i->second.toInt();
		}else if ([itemName isEqualToString:@"hdvd"]){
			self.hdVideo = i->second.toBool();
		}else if ([itemName isEqualToString:@"tves"]){
			self.episode = i->second.toInt();
		}else if ([itemName isEqualToString:@"©day"]){
			NSDate *date;
			NSString *dateString = [self stringFromMP4Item:i->second];
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
			date = [dateFormatter dateFromString:dateString];
			if (!date) {
				// Try simpler date format
				[dateFormatter setDateFormat:@"yyyy-MM-dd"];
				date = [dateFormatter dateFromString:dateString];
				if (!date) {
					// Try just the year
					[dateFormatter setDateFormat:@"yyyy"];
					date = [dateFormatter dateFromString:dateString];
				}
			}
			self.releaseDate = date;
		}else {
			TagLib::StringList stringList = i->second.toStringList();
			NSLog(@"Name = %@, type = %d", itemName, item.atomDataType());

			for(TagLib::StringList::ConstIterator j = stringList.begin(); j != stringList.end(); ++j) {
				NSString *someString = [NSString stringWithCString:j->toCString(true) encoding:NSUTF8StringEncoding];
				NSLog(@"%@", someString);
			}
			
		}
	}
}

- (NSString *)stringFromMP4Item: (TagLib::MP4::Item)anItem {
	NSString *aString;
	TagLib::StringList stringList = anItem.toStringList();
	for(TagLib::StringList::ConstIterator j = stringList.begin(); j != stringList.end(); ++j) {
		aString = [NSString stringWithCString:j->toCString(true) encoding:NSUTF8StringEncoding];
		if (aString) {
			break;
		}
	}
	
	return aString;
}

- (void)writeTags {
	TagLib::MP4::File mp4File([self.filePath cStringUsingEncoding:NSUTF8StringEncoding]);
	
	if (!mp4File.isValid()) {
		NSLog(@"Could not open file %@", self.filePath);
		return;
	}
	
	TagLib::MP4::Tag *tag = mp4File.tag();
	
	TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
	
	BOOL hasChanges = NO;
	if (self.showName) {
		hasChanges = YES;
		tag->itemListMap()["tvsh"] = TagLib::StringList([self.showName cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.releaseDate) {
		hasChanges = YES;
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
		NSString *releaseDateString = [dateFormatter stringFromDate:self.releaseDate];
		tag->itemListMap()["\251day"] = TagLib::StringList([releaseDateString cStringUsingEncoding:NSUTF8StringEncoding]);
	}
	
	if (self.title) {
		hasChanges = YES;
		tag->itemListMap()["\251nam"] = TagLib::StringList([self.title cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.episodeDescription) {
		hasChanges = YES;
		tag->itemListMap()["desc"] = TagLib::StringList([self.episodeDescription cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.longDescription) {
		hasChanges = YES;
		tag->itemListMap()["ldes"] = TagLib::StringList([self.longDescription cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.showDescription) {
		hasChanges = YES;
		tag->itemListMap()["sdes"] = TagLib::StringList([self.showDescription cStringUsingEncoding:NSUTF8StringEncoding]);
	}
	
	if (self.genre) {
		hasChanges = YES;
		tag->itemListMap()["\251gen"] = TagLib::StringList([self.genre cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.copyright) {
		hasChanges = YES;
		tag->itemListMap()["cprt"] = TagLib::StringList([self.copyright cStringUsingEncoding:NSUTF8StringEncoding]);
	}

	if (self.network) {
		hasChanges = YES;
		tag->itemListMap()["tvnn"] = TagLib::StringList([self.network cStringUsingEncoding:NSUTF8StringEncoding]);
	}
	
	if (self.episodeID) {
		hasChanges = YES;
		tag->itemListMap()["tven"] = TagLib::StringList([self.episodeID cStringUsingEncoding:NSUTF8StringEncoding]);
	}
	
	if (self.season) {
		hasChanges = YES;
		tag->itemListMap()["tvsn"] = TagLib::MP4::Item((long long)self.season);
	}
	
	if (self.episode) {
		hasChanges = YES;
		tag->itemListMap()["tves"] = TagLib::MP4::Item((long long)self.episode);
	}

	if (self.hdVideo) {
		hasChanges = YES;
		tag->itemListMap()["hdvd"] = TagLib::MP4::Item(self.hdVideo);
	}

	if (self.mediaType != TKMediaTypeUnknown) {
		hasChanges = YES;
		tag->itemListMap()["stik"] = TagLib::MP4::Item((long long)self.mediaType);
	}

	if (self.coverArt) {
		hasChanges = YES;
		NSData *coverArtData = [NSBitmapImageRep representationOfImageRepsInArray:self.coverArt.representations usingType:NSJPEGFileType properties:nil];

		TagLib::MP4::CoverArt coverArt((TagLib::MP4::CoverArt::Format) 0x0D, TagLib::ByteVector((const char *)coverArtData.bytes, (unsigned int)coverArtData.length));
		TagLib::MP4::CoverArtList coverArtList;
		coverArtList.append(coverArt);

		tag->itemListMap()["covr"] = TagLib::MP4::Item(coverArtList);
	}
	
	if(hasChanges) {
		if(!tag->save()){
			NSLog(@"ERROR: Could not save file");
		}
	}
}

@end
