//
//  TKFile.h
//  TagLibKit
//
//  Created by Cory Powers on 5/1/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef NS_ENUM
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#endif

NS_ENUM (NSInteger, TKMediaType){
	TKMediaTypeUnknown = 0,
	TKMediaTypeMusic = 1,
	TKMediaTypeAudiobook = 2,
	TKMediaTypeMusicVideo = 6,
	TKMediaTypeMovie = 9,
	TKMediaTypeTVShow = 10,
	TKMediaTypeBooklet = 11,
	TKMediaTypeRingtone = 14
};

@interface TKFile : NSObject
@property (nonatomic, strong) NSString *showName;
@property (nonatomic, assign) enum TKMediaType mediaType;
@property (nonatomic, strong) NSDate *releaseDate;
@property (nonatomic, strong) NSImage *coverArt;
@property (nonatomic, assign) NSInteger episode;
@property (nonatomic, assign) NSInteger season;
@property (nonatomic, strong) NSString *episodeID;
@property (nonatomic, assign) BOOL hdVideo;
@property (nonatomic, strong) NSString *episodeDescription;
@property (nonatomic, strong) NSString *longDescription;
@property (nonatomic, strong) NSString *showDescription;
@property (nonatomic, strong) NSString *network;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *copyright;
@property (nonatomic, strong) NSString *genre;

@property (nonatomic, readonly) NSString *filePath;

- (id)initWithFile: (NSString *)filePath;
- (void)readTags;
- (void)writeTags;
@end
